#!/usr/bin/env sh

#   Harb's i3-KDE setup
#   /   /   /   /   /
#    /   /   /   /

# mostly yoinked from here: https://github.com/heckelson/i3-and-kde-plasma


yay -S  --needed --noconfirm i3-wm plasma

mkdir $HOME/.config/systemd
mkdir $HOME/.config/systemd/user

echo "
[Unit]
Description=Launch Plasma with i3
Before=plasma-workspace.target

[Service]
ExecStart=/usr/bin/i3
Restart=on-failure

[Install]
WantedBy=plasma-workspace.target
" > $HOME/.config/systemd/user/plasma-i3.service

systemctl mask plasma-kwin_x11.service --user
systemctl enable plasma-i3 --user

# echo "
# # Plasma compatibility improvements
# for_window [window_role="pop-up"] floating enable
# for_window [window_role="task_dialog"] floating enable

# for_window [class="yakuake"] floating enable
# for_window [class="systemsettings"] floating enable
# for_window [class="plasmashell"] floating enable;
# for_window [class="Plasma"] floating enable; border none
# for_window [title="plasma-desktop"] floating enable; border none
# for_window [title="win7"] floating enable; border none
# for_window [class="krunner"] floating enable; border none
# for_window [class="Kmix"] floating enable; border none
# for_window [class="Klipper"] floating enable; border none
# for_window [class="Plasmoidviewer"] floating enable; border none
# for_window [class="(?i)*nextcloud*"] floating disable
# for_window [class="plasmashell" window_type="notification"] border none, move position 70 ppt 81 ppt
# no_focus [class="plasmashell" window_type="notification"]

# for_window [title="Desktop @ QRect.*"] kill; floating enable; border none

# bindsym XF86AudioRaiseVolume exec --no-startup-id qdbus org.kde.kglobalaccel /component/kmix invokeShortcut "increase_volume"
# bindsym XF86AudioLowerVolume exec --no-startup-id qdbus org.kde.kglobalaccel /component/kmix invokeShortcut "decrease_volume"
# bindsym XF86AudioMute exec --no-startup-id qdbus org.kde.kglobalaccel /component/kmix invokeShortcut "mute"
# bindsym XF86AudioMicMute exec --no-startup-id qdbus org.kde.kglobalaccel /component/kmix invokeShortcut "mic_mute"

# bindsym $mod+Shift+e exec --no-startup-id qdbus6 org.kde.ksmserver /KSMServer org.kde.KSMServerInterface.logout -1 -1 -1
# " >> $HOME/.config/i3/config

echo "Please reboot into KDE-X11 and disable conflicting Shortcuts e.g. every mod+NUMBER"
