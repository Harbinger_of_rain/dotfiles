{ config, pkgs, ... }:

{
  targets.genericLinux.enable = true;


  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager release notes.
  home.stateVersion = "22.11";

  fonts.fontconfig.enable = true;

  nix.settings.max-jobs = "auto";
  nix.settings.experimental-features = "nix-command flakes";

  nix.package = pkgs.nix;
  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = [
    # libs
    pkgs.glibcLocales
    pkgs.nerdfonts
    pkgs.imagemagick

    # x11

    # wayland
    pkgs.rofi-wayland
    pkgs.wofi
    pkgs.swaybg
    # pkgs.swaylock-effects # BROKE: password not recognised??
    pkgs.waybar
    pkgs.xdg-desktop-portal-hyprland
    pkgs.hyprpicker
    pkgs.mpvpaper
    pkgs.wayshot
    # pkgs.sddm

    # both
    # pkgs.kitti3
    pkgs.variety

    pkgs.arion
    pkgs.node2nix

    pkgs.vim
    pkgs.screen
    pkgs.cpulimit
    pkgs.htop
    pkgs.broot
    pkgs.neofetch

    pkgs.usbutils

    # fun
    pkgs.cmatrix
    pkgs.lolcat
    pkgs.hollywood
    pkgs.oneko
    pkgs.pipes
    pkgs.sonic-visualiser

    pkgs.bottles

    # pkgs.git
    pkgs.fossil

    # social
    pkgs.armcord
    # pkgs.discord
    pkgs.telegram-desktop

    # pkgs.kitty #no worky

    # music
    pkgs.mpd
    pkgs.mpc-cli
    pkgs.ncmpcpp

    # ui
    # pkgs.etcher
    pkgs.gparted
    pkgs.qdirstat
    pkgs.obsidian
    pkgs.vscode
    pkgs.deluge
    pkgs.xfce.mousepad
    pkgs.libreoffice-fresh
    pkgs.pcmanfm
    # pkgs.dolphin
    pkgs.xfce.thunar # files
    pkgs.sxiv # img
    pkgs.zathura # pdf

    pkgs.firefox
    # (pkgs.firefox.override {})
    # pkgs."firefox-117.0.1"

    # gaming
    # pkgs.steam #no worky
    pkgs.prismlauncher
    pkgs.sc-controller # controller mgr
    # pkgs.lighthouse-steamvr
    pkgs.gamehub
    # (pkgs.factorio.override { username = "HarbAlarm"; token = "REDACTED"; })

    # creative
    # pkgs.blender
    pkgs.unityhub
    pkgs.gimp
    pkgs.inkscape

    # pkgs.jre8


    # chaotic nyx packages
    # pkgs.firedragon
    
    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # "~/.config/home-manager/flake.nix".source = ./flake.nix

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';

    # ".config/inkscape" = {
    # source = ./inkscape;
    # recursive = true;
    # };
  };

  programs.bash.enable = true;
  # programs.fish.enable = true;

  # You can also manage environment variables but you will have to manually
  # source
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  # or
  #  /etc/profiles/per-user/elia/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.
  home.sessionVariables = {
    PATH="$PATH:/home/$USER/.nix-profile/bin";
    # XDG_DATA_DIRS="$HOME/.local/share/applications:$HOME/.nix-profile/share";
    NIXPKGS_ALLOW_UNFREE = 1;

    EDITOR = "vim";
    SHELL = "fish";
    TERMINAL = "kitty fish";
    TERMINALEXEC = "kitty";
    BROWSER = "vivaldi-stable";

    QT_QPA_PLATFORMTHEME = "qt5ct";
    # GTK2_RC_FILES = "$HOME/.gtkrc-2.0";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  # programs.home-manager.useGlobalPkgs = true;

  # programs.xwayland.enable = true;

  programs.git = {
    enable = true;
    userName = "Elia";
    userEmail = "Elia@Fryzen.net";
    aliases = {
      cum = "commit";
    };
  };

  # services.sddm.enable = true;
  services.syncthing.enable = true;

  nixpkgs.config = {
    allowUnfree = true;
    allowUnfreePredicate = _: true;
  };

  gtk = {
    enable = true;
    # theme.name = "adw-gtk3";
    # theme.name = "Adapta-Nokto-Eta-Maia";
    # theme.name = "breeze";
    # theme.package = pkgs.breeze-gtk;
    # cursorTheme.name = "Bibata-Modern-Ice";
    # cursorTheme.name = "xcursor-breeze";
    # iconTheme.name = "GruvboxPlus";
    # iconTheme.package = pkgs.maia-icon-theme;

    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.papirus-icon-theme;
    };

    theme = {
      name = "palenight";
      package = pkgs.palenight-theme;
      # name = "breeze-gtk";
      # package = pkgs.breeze-gtk;
    };

    cursorTheme = {
      name = "Numix-Cursor";
      package = pkgs.numix-cursor-theme;
    };

    gtk3.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=1
      '';
    };
    gtk4.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=1
      '';
    };
  };
  home.sessionVariables.GTK_THEME = "palenight";

  # qt = {
  #   enable = true;

  # }

  # xdg.portal = {
  #   enable = true;
  #   extraPortals = with pkgs; [xdg-desktop-portal-gtk xdg-desktop-portal-hyprland];
  # };


  # -- Default Apps
  xdg.mime.enable = true;
  xdg.mimeApps.enable = true;
  xdg.mimeApps.defaultApplications = {
    "text/plain" = [ "org.xfce.mousepad.desktop" ];
    "application/pdf" = [ "zathura" "epdfview.desktop" "firefox.desktop" ];
    
    "image/*" = [
      "sxiv"
      "viewnior.desktop"
      "gpicview.desktop"
      "gimp.desktop"
    ];
    "image/png" = [
      "sxiv.desktop"
    ];

    "video/png" = [ "mpv" ];
    "video/*" = [ "mpv" ];

    # x-scheme-handler/mailto=userapp-Thunderbird-3IU9RZ.desktop
    # message/rfc822=userapp-Thunderbird-3IU9RZ.desktop

    "x-scheme-handler/*" = [ "firedragon.desktop" "firefox.desktop" ];
    "text/html" = [ "firefox.desktop" ];
    "application/x*" = [ "firefox.desktop" ];
    # x-scheme-handler/jetbrains=jetbrains-toolbox.desktop

    "application/x-blender" = [ "userapp-blender-TV9YB2.desktop" ];
  };
  xdg.mimeApps.associations.added  = {
    "x-scheme-handler/*" = [ "firefox.desktop" ];
    # application/x-bittorrent=transmission-gtk.desktop;
  };
  xdg.mimeApps.associations.removed  = {
    "text/plain" = [ "geany.desktop" ];
  };
}
