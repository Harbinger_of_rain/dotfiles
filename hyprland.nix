{ config, pkgs, ... }: {
#   programs.hyprland = {
#   package = hyprland.packages.${pkgs.system}.hyprland.override {
#     legacyRenderer = true;
#   };
# };

  wayland.windowManager.hyprland = {
    # enable = false;
    # package = inputs.hyprland.packages.${pkgs.system}.hyprland;
  };

  wayland.windowManager.hyprland.extraConfig = (builtins.readFile ./hyprland.conf) +
  ''
  ${builtins.concatStringsSep "\n" (builtins.genList (
        x: let
          ws = let
            c = (x + 1) / 10;
          in
            builtins.toString (x + 1 - (c * 10));
        in ''
          bind = $mod, ${ws}, workspace, ${toString (x + 1)}
          bind = $mod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}
          bind = $mod CTRL, ${ws}, movetoworkspacesilent, ${toString (x + 1)}
        ''
      )
      10)}
  '';

  # wayland.windowManager.hyprland.plugins = [
  #   inputs.hyprland-plugins.packages.${pkgs.system}.hyprbars
  #   "/absolute/path/to/plugin.so"
  # ];
}