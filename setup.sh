#!/usr/bin/env sh
#2019/01/19
#Elia Fry

# todo: opts - https://stackoverflow.com/questions/4609668/override-a-variable-in-a-bash-script-from-the-command-line

# set default values

# argument variables
homeCopy=false
rootCopy=false
updatevivaldi=false
reload=false
hostname=""

sourceDir=$(dirname "$0")
cd $sourceDir

# function to report error messages
errMsg()
	{
	echo ""; echo $1; echo ""; echo usage1; exit 1
	}


# test for correct number of arguments and get values
if [ $# -eq 0 ]
	then
	# help information
   echo ""
   echo usage2
   exit 0
elif [ $# -gt 16 ]
	then
	errMsg "--- TOO MANY ARGUMENTS WERE PROVIDED ---"
else
	while [ $# -gt 0 ]
		do
			# get parameter values
			case "$1" in
		        -h)    # help information
					   echo ""
					   echo usage2
					   exit 0
					   ;;
				-n)		# get hostname
						shift  # to get the next parameter
						errorMsg="--- INVALID HOSTNAME SPECIFICATION ---"
						hostname=`echo "$1" | tr "[:upper:]" "[:lower:]"`
						homeCopy=true
						case "$hostname" in
							default|d) hostname="default" ;;

							viviante|vivi) hostname="viviante" ;;
							coldbox|c) hostname="coldbox" ;;
							laptop|l) hostname="laptop" ;;
							insor|i) hostname="insor" ;;
							fair-rates|f) hostname="fair-rates" ;;
							vm) hostname="vm" ;;
							insor|i) hostname="insor" ;;
							*) errMsg "--- hostname=$hostname not in config, manual setup required! ---" ;;
						esac
				;;
				--root)
						rootCopy=true
				;;
		        -uv|--updatevivaldi)
						updatevivaldi=true
				;;
				--reload)
						reload=true
				;;
				-)    # STDIN and end of arguments
					   break
				;;
				-*)    # any other - argument
					   errMsg "--- UNKNOWN OPTION ---"
				;;
		     	*)    # end of arguments
					   break
				;;
			esac
			shift   # next option
	done
fi


if ($homeCopy); then
    echo "executing home copy for $hostname"

    ./script/recursive_override.sh "./host/0_default/home" $HOME

    ./script/recursive_append.sh "./host/$hostname/home" $HOME

	echo ""
	echo "home copy complete"
    ./script/hr.sh

    case "$hostname" in
    	default) #hostname="coldbox"
            #sed -i 's/replaceWithMonitorDescriptor/DP-4/g' ~/.config/polybar/config
        ;;
        viviante) hostname="viviante"
                sed -i 's/replaceWithMonitorDescriptor/HDMI-A-0/g' ~/.config/polybar/config
        ;;
    	coldbox) hostname="coldbox"
            sed -i 's/replaceWithMonitorDescriptor/DP-3/g' ~/.config/polybar/config
        ;;
    	laptop) hostname="laptop"
            sed -i 's/replaceWithMonitorDescriptor/eDP1/g' ~/.config/polybar/config
        ;;
    	insor) hostname="insor"
            sed -i 's/replaceWithMonitorDescriptor/eDP-1/g' ~/.config/polybar/config
        ;;
    	fair-rates) hostname="fair-rates"
            sed -i 's/replaceWithMonitorDescriptor/DP-1/g' ~/.config/polybar/config
        ;;
    	vm) hostname="vm"
            sed -i 's/replaceWithMonitorDescriptor/Virtual1/g' ~/.config/polybar/config
        ;;
    	*) errMsg "--- hostname=$hostname not in config, manual setup required! ---" ;;
    esac
fi

if ($rootCopy); then
    echo "executing root copy for $hostname"

    sudo ./script/recursive_override.sh "./host/0_default/root" /

    sudo ./script/recursive_append.sh "./host/$hostname/root" /

	echo ""
	echo "root copy complete"
    ./script/hr.sh

    # sudo systemctl restart systemd-logind
fi

if ($updatevivaldi); then
    python ./setupScripts/vSetHomePrefs.py

    if ($rootCopy); then
#    sudo sh -c "grep -qxF '@import \"custom.css\";' /opt/vivaldi/resources/vivaldi/style/common.css || sed -i '1s/^/@import \"custom.css\";\n/' /opt/vivaldi/resources/vivaldi/style/common.css"
      sudo python ./setupScripts/vSetOptPrefs.py

      sudo /opt/vivaldi/update-ffmpeg
      sudo /opt/vivaldi/update-widevine
      sudo /opt/vivaldi-snapshot/update-ffmpeg
      sudo /opt/vivaldi-snapshot/update-widevine
    fi
fi

if ($reload); then
    sleep .3
    # case "$DESKTOP_SESSION" in # could be kde + i3
	if wmctrl -m | grep i3 > /dev/null; then
		i3-msg restart > /dev/null && echo i3 reload successful
	elif wmctrl -m | grep awesome > /dev/null; then
		awesome -r > /dev/null && echo awesome reload successful
    else
		notify-send -u critical "cannot reload unknown windowmanager, reload it manually"
	fi
fi

echo "ran setup.sh for $hostname"
