#!/usr/bin/env sh
#2019/01/17
#Elia Fry

BASEDIR=$(dirname "$0")
cr='\033[0;31m'
cg='\033[0;32m'
cb='\033[0;36m'
cn='\033[0m'

cd $BASEDIR

if ! [ "$EUID" -ne 0 ]
  then echo -e "${cr}Please don't run as root${cn}"
  exit
fi

if ! pacman -Qi polybar > /dev/null; then
  echo -e "${cb}Installing polybar${cn}"
  sudo pacman -S --noconfirm polybar
else
  echo -e "${cb}Polybar is already installed.${cn}"
fi

mkdir ~/.config/polybar
cp launch.sh ~/.config/polybar/launch.sh
cp config ~/.config/polybar/config
chmod +x ~/.config/polybar/launch.sh

echo -e "${cb}For i3, remove/disable all \"bar { }\""
echo -e "and add: exec_always --no-startup-id \$HOME/.config/polybar/launch.sh${cn}"
