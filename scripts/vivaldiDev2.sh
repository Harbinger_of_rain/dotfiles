#!/usr/bin/env sh

screen -dmS vivDev bash -c "vivaldi-snapshot --debug-packejd-apps --silent-debugger-extension-api --user-data-dir=/tmp/vivDev && rm -r /tmp/vivDev"
