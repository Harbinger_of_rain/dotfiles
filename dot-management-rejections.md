

# yadm
yadm is basically just a wrapper for git + basic templating which is all saved directly in a users home dir, even differing files
the repo is at .local/share/yadm but no actual files are present
using differing configs in yadm means you get to have 3 files for any one replaced one e.g.:
test##default
test##hostname.secondary
test

# dotbot:
The setup is quite nice in that the git submodule is integrated into the project and only git needs to be installed
But dotbot itself seems quite ovecomplicated, especially since it has no integrated way of managing differing configurations:


Symlinking setups like dotbot seem ill-equipped for my use case where i prefer to move template files to a destination before applying differing settings like monitor setups for polybar


