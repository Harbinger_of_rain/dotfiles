# Dotfiles

This Repository contains my dotfiles and set-up scripts.

Use them as you wish, at your own risk. And dont share them without giving credit to its creators.

<br/>

TODO:
[ ] central config file to avoid "updated display"
    [ ] add config to check whether installer.sh had been run already
[ ] central credential file?
[ ] nixos config? 


<img align="left" border="0" src="https://gitlab.com/Harbinger_of_rain/dotfiles/raw/master/.meta/2019-03-08-183224_2560x1080_scrot.png">

<br/>

### inspiration/code snippets:

https://www.reddit.com/r/unixporn/comments/ah5vkk/i3gaps_life_is_a_glitch_glitchart_with_glitchout/

https://github.com/pavanjadhaw/betterlockscreen

https://www.reddit.com/r/unixporn/comments/ab5xwy/herbstluftwm_happy_new_year/
