import vPrefFunctions as v
import os, subprocess


def setOptPrefs(prefs):
    v.setp(prefs, ['vivaldi', 'address_bar', 'omnibox', 'enabled', 'default'], True)
    v.setp(prefs, ['vivaldi', 'address_bar', 'omnibox', 'show_bookmarks', 'default'], False)
    v.setp(prefs, ['vivaldi', 'address_bar', 'omnibox', 'show_typed_history', 'default'], True)
    v.setp(prefs, ['vivaldi', 'address_bar', 'omnibox', 'show_top_sites', 'default'], True)

    v.setp(prefs, ['vivaldi', 'address_bar', 'search', 'enabled', 'default'], False)

    # setp(prefs, ['vivaldi', 'bookmarks', 'deleted_partners', 'default'], [])
    v.setp(prefs, ['vivaldi', 'bookmarks', 'open_in_new_tab', 'default'], False)

    v.setp(prefs, ['vivaldi', 'downloads', 'start_automatically', 'default'], True)
    v.setp(prefs, ['vivaldi', 'downloads', 'update_default_download_when_saving_as', 'default'], False)

    v.setp(prefs, ['vivaldi', 'homepage', 'default'], 'https://fryzen.net')
    v.setp(prefs, ['vivaldi', 'homepage_cache', 'default'], 'https://fryzen.net')

    v.setp(prefs, ['vivaldi', 'razer_chroma', 'has_shown_promotion', 'default'], True)

    v.setp(prefs, ['vivaldi', 'incognito', 'show_intro', 'default'], False)

    v.setp(prefs, ['vivaldi', 'mouse_wheel', 'tab_switch', 'default'], True)

    # v.setp(prefs, ['vivaldi', 'panels', 'as_overlay', 'auto_close', 'default'], True)
    # v.setp(prefs, ['vivaldi', 'panels', 'as_overlay', 'enabled', 'default'], True)

    # v.setp(prefs, ['vivaldi', 'popups', 'show_in_tab', 'default'], True)

    # v.setp(prefs, ['vivaldi', 'privacy', 'autofill', 'server_assist', 'default'], False)

    # v.setp(prefs, ['vivaldi', 'startpage', 'background', 'color', 'default'], "#cccccc")
    # v.setp(prefs, ['vivaldi', 'startpage', 'background', 'color_custom', 'default'], "#222222")

    # v.setp(prefs, ['vivaldi', 'startpage', 'image', 'enable', 'default'], True)
    # v.setp(prefs, ['vivaldi', 'startpage', 'image', 'path', 'default'], "./../resources/bg.jpg")
    # v.setp(prefs, ['vivaldi', 'startpage', 'image', 'path_custom', 'default'], "./../resources/bg.jpg")
    # v.setp(prefs, ['vivaldi', 'startpage', 'image', 'repeat', 'default'], False)
    # v.setp(prefs, ['vivaldi', 'startpage', 'image', 'stretch', 'default'], False)

    # v.setp(prefs, ['vivaldi', 'startpage', 'speed_dial', 'columns', 'default'], 4)

    v.setp(prefs, ['vivaldi', 'startup', 'check_is_default', 'default'], False)
    v.setp(prefs, ['vivaldi', 'startup', 'remember_full_screen', 'default'], True)

    # v.setp(prefs, ['vivaldi', 'tabs', 'bar', 'width', 'default'], 100)
    # v.setp(prefs, ['vivaldi', 'tabs', 'bar', 'height', 'default'], 30)
    # v.setp(prefs, ['vivaldi', 'tabs', 'bar', 'position', 'default'], "left")

    # v.setp(prefs, ['vivaldi', 'tabs', 'cycle_by_recent_order', 'default'], False)
    v.setp(prefs, ['vivaldi', 'tabs', 'minimize', 'default'], True)

    # v.setp(prefs, ['vivaldi', 'tabs', 'new_page', 'link', 'default'], "startpage")

    # Options: startpage, homepage, blankpage, extension, custom
    v.setp(prefs, ['vivaldi', 'tabs', 'new_page', 'custom_url', 'default'], "https://fryzen.net")

    # Options: rightofcurrent, directrightofcurrent, alwayslast, openintabstackwithrelated
    v.setp(prefs, ['vivaldi', 'tabs', 'new_placement', 'default'], "openintabstackwithrelated")

    # v.setp(prefs, ['vivaldi', 'tabs', 'selection', 'enabled', 'default'], True)
    # v.setp(prefs, ['vivaldi', 'tabs', 'selection', 'include_active', 'default'], True)

    v.setp(prefs, ['vivaldi', 'tabs', 'show_close_button', 'default'], False)

    v.setp(prefs, ['vivaldi', 'tabs', 'stacking', 'allow_dnd', 'default'], True)
    # v.setp(prefs, ['vivaldi', 'tabs', 'stacking', 'allow_rename', 'default'], True)
    v.setp(prefs, ['vivaldi', 'tabs', 'stacking', 'dnd_delay', 'default'], 0)    # default 250
    v.setp(prefs, ['vivaldi', 'tabs', 'stacking', 'open_in_current', 'default'], True)

    v.setp(prefs, ['vivaldi', 'tabs', 'thumbnails', 'default'], False)
    v.setp(prefs, ['vivaldi', 'tabs', 'visual_switch', 'enable', 'default'], True)
    v.setp(prefs, ['vivaldi', 'tabs', 'visual_switch', 'list_layout', 'default'], True)

    # Todo: theme

    v.setp(prefs, ['vivaldi', 'toolbars', 'navigation', 'default'], [
              "Back",
              "Forward",
              "Rewind",
              # "FastForward",
              "Reload",
              "Home"
            ])
    v.setp(prefs, ['vivaldi', 'toolbars', 'status', 'default'], [
              "PanelToggle",
              "SyncStatus",
              "StatusInfo",
              "TaskList",
              "VersionInfo",
              "CaptureImages",
              "TilingToggle",
              "ImagesToggle",
              "PageActions",
              "Zoom"
            ])

    # v.setp(prefs, ['vivaldi', 'webpages', 'capture', 'directory', 'default'], '~/Downloads')
    # Options: file, clipboard
    # v.setp(prefs, ['vivaldi', 'webpages', 'capture', 'save_location', 'default'], 'file')
    # v.setp(prefs, ['vivaldi', 'webpages', 'capture', 'save_file_pattern', 'default'], '$timestamp $host $shortid')

    # v.setp(prefs, ['vivaldi', 'welcome', 'seen_pages', 'default'], [])
    # v.setp(prefs, ['vivaldi', 'welcome', 'read_pages', 'default'], [])

    # v.setp(prefs, ['vivaldi', 'rss', 'settings', 'default'], [])
    # v.setp(prefs, ['vivaldi', 'rss', 'show_notifications', 'default'], True)

    # v.setp(prefs, ['vivaldi', 'windows', 'linux_alt_controls', 'default'], False)
    v.setp(prefs, ['vivaldi', 'windows', 'use_native_decoration', 'default'], True)
    v.setp(prefs, ['vivaldi', 'windows', 'show_window_close_confirmation_dialog', 'default'], False)

    # v.setp(prefs, ['vivaldi', 'system', 'has_desktop_wallpaper_protocol', 'default'], False)
    # v.setp(prefs, ['vivaldi', 'system', 'has_desktop_wallpaper_protocol', 'default_linux'], False)
    # v.setp(prefs, ['vivaldi', 'system', 'has_desktop_wallpaper_protocol', 'default_mac'], False)
    v.setp(prefs, ['vivaldi', 'system', 'desktop_theme_color', 'default'], "dark")
    v.setp(prefs, ['vivaldi', 'system', 'desktop_theme_color', 'default_linux'], "dark")
    v.setp(prefs, ['vivaldi', 'system', 'desktop_theme_color', 'default_mac'], "dark")

    v.setp(prefs, ['vivaldi', 'system', 'show_exit_confirmation_dialog', 'default'], False)


opt_prefsdef = '/opt/vivaldi/resources/vivaldi/prefs_definitions.json'
opt_snap_prefsdef = '/opt/vivaldi-snapshot/resources/vivaldi/prefs_definitions.json'

home_prefs = '~/.config/vivaldi/Default/Preferences'
home_snap_prefs = '~/.config/vivaldi-snapshot/Default/Preferences'

local_prefs = 'Preferences.json'
local_prefsdef = 'prefs_definitions.json'


try:
    prefJson = v.readfile(opt_prefsdef)
    setOptPrefs(prefJson)
    v.writefile(opt_prefsdef, prefJson)

    prefJson = v.readfile(opt_snap_prefsdef)
    setOptPrefs(prefJson)
    v.writefile(opt_snap_prefsdef, prefJson)
except IOError and TypeError as ioE:
    print("couldn't write to /opt, are you missing sudo privileges?")
