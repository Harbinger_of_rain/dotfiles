import vPrefFunctions as v
from pathlib import Path


def setHomePrefs(prefs):
    v.setp(prefs, ['devtools', 'preferences', 'uiTheme'], "\"dark\"")   # 820

    v.setp(prefs, ['download', 'default_directory'], "/home/harbinger/Downloads")   # 825
    v.setp(prefs, ['download', 'directory_upgrade'], False)   # 826
    v.setp(prefs, ['enable_do_not_track'], True)   # 828




    v.setp(prefs, ['vivaldi', 'address_bar', 'extensions', 'show_toggle'], True)  # 13461
    # v.setp(prefs, ['vivaldi', 'appearance', 'css_ui_mods_directory'], "/home/harbinger/.config/vivaldiCSS")  # 13481
    v.setp(prefs, ['vivaldi', 'bookmarks', 'language'], "en-GB")  # 13568
    v.setp(prefs, ['vivaldi', 'experiments'], [
      "css_mods",
      "glorified_find_in_page_results"
    ])  # 13615

    v.setp(prefs, ['vivaldi', 'startup', 'check_is_default'], False)  # 13702
    # v.setp(prefs, ['vivaldi', 'system', 'show_exit_confirmation_dialog'], False)

    v.setp(prefs, ['vivaldi', 'tabs', 'cycle_by_recent_order'], False)
    v.setp(prefs, ['vivaldi', 'tabs', 'never_close_last'], False)



    v.setp(prefs, ['vivaldi', 'welcome', 'seen_pages'], [
        "welcome",
        "import_data",
        "personalize",
        "tabs",
        "sync",
        "touch"
      ])   # 13830


home = str(Path.home())

opt_prefsdef = '/opt/vivaldi/resources/vivaldi/prefs_definitions.json'
opt_snap_prefsdef = '/opt/vivaldi-snapshot/resources/vivaldi/prefs_definitions.json'

home_prefs = home + '/.config/vivaldi/Default/Preferences'
home_snap_prefs = home + '/.config/vivaldi-snapshot/Default/Preferences'

local_prefs = 'Preferences.json'
local_prefsdef = 'prefs_definitions.json'


try:
    # prefJson = v.readfile(local_prefs)
    # setHomePrefs(prefJson)
    # v.writefile(local_prefs, prefJson)

    prefJson = v.readfile(home_prefs)
    setHomePrefs(prefJson)
    v.writefile(home_prefs, prefJson)

    prefJson = v.readfile(home_snap_prefs)
    setHomePrefs(prefJson)
    v.writefile(home_snap_prefs, prefJson)
except IOError and PermissionError as e:
    print("couldn't write to /opt, are you missing sudo privileges?")
    print("Error:", e)
