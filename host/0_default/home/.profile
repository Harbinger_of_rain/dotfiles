
export PATH="$PATH:/home/$USER/.nix-profile/bin:/home/$USER/.local/bin"
export XDG_DATA_DIRS="$HOME/.local/share/applications:$HOME/.nix-profile/share"

export NIXPKGS_ALLOW_UNFREE=1

export QT_QPA_PLATFORMTHEME="qt5ct"
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

export TERMINAL=kitty fish
export TERMINALEXEC=kitty
export EDITOR=vim
export BROWSER=vivaldi-stable
