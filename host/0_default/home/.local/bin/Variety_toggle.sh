#!/usr/bin/env sh
#2024/08/17
#Elia Fry

if pgrep -x "variety" > /dev/null; then
	killall variety &
	nitrogen --restore &
else
	killall nitrogen &
	variety &
fi

