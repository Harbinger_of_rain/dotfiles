#!/usr/bin/env bash

pw=$(zenity --password)

declare -a drives=("bdrive" "gdrive")
echo ${drives[*]}

yay -Q screen
if [ $? -eq 1 ]
then
    zenity --warning --text "please install screen"
    exit 1
fi

declare -a dirs=$(ls ~/rclone/)
if [ $? -eq 2 ]
then
    mkdir ~/rclone/
    for dir in "${drives[@]}"
    do
        mkdir ~/rclone/"$dir"
    done
else
    for dir in "${drives[@]}"
    do
        drives=( "${drives[@]/${dir}/}" )
    done
    for dir in "${drives}"
    do
        if [ ! -d ~/rclone/"$dir" ]
        then
        mkdir ~/rclone/"$dir"
        fi
    done
fi

if ! mount | grep ~/rclone > /dev/null; then
    screen -dmS rclone-gdrive bash -c "echo -e $pw | rclone --vfs-cache-mode writes mount gdrive: ~/rclone/gdrive/"
    screen -dmS rclone-bdrive bash -c "echo -e $pw | rclone --vfs-cache-mode writes mount bdrive: ~/rclone/bdrive/"
fi

# kill by using:
# screen -S rclone-gdrive -X quit
# screen -S rclone-bdrive -X quit
