#!/usr/bin/env bash

profiles=$(ls ~/.config/falkon/profiles/)
profiles="$profiles private"

echo $profiles
selection=`printf "%s\n" ${profiles} | rofi -theme android_notification -font "hack 10" -i -p "${promt}" -dmenu`

if [ ! ${selection} ]; then
    echo "no profile selected, exiting"
    exit 0
elif [ ${selection} == "private" ]; then
    bash -c "falkon -r -i --wmclass falkon-private"
    exit 0
elif [[ ! "${profiles}" =~ "${selection}" ]]; then
    echo new
    cp -r ~/.config/falkon/profiles/preset ~/.config/falkon/profiles/$selection
fi

echo "falkon -r -p $selection --wmclass falkon-$selection"
bash -c "falkon -r -p $selection --wmclass falkon-$selection"
