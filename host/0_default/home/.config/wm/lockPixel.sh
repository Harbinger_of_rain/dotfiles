#!/bin/bash
 
#depends on: imagemagick, i3lock, scrot
#https://www.reddit.com/r/unixporn/comments/3358vu/i3lock_unixpornworthy_lock_screen/cqilnv3
 
IMAGE=/tmp/screen.png
ICON=
scrot $IMAGE
convert $IMAGE -scale 5% -scale 2000% $IMAGE
convert $IMAGE $ICON -gravity center -composite -matte $IMAGE
i3lock -u -i $IMAGE
rm $IMAGE
