#!/usr/bin/env sh

#~/.config/wm/lockPixel.sh
#~/.config/wm/lockPixelGlitch.sh
swaylock -c 33333333 -e -f --indicator --indicator-idle-visible --indicator-thickness 7 -F --clock --grace 0  --fade-in 1.5 -S \
\#--effect-pixelate 10 \
--effect-blur 7x5 \
\#--effect-vignette 0.5:0.5 \
--key-hl-color ff00ff --caps-lock-key-hl-color 00ffff \
--text-color ffffff --text-clear-color ffffff00 --text-wrong-color ffffff00 --text-ver-color 00000000 \
--ring-color 550055 --ring-ver-color ff00ff --ring-clear-color ffffff --ring-wrong-color ff0000 \
--inside-color 00000099 --inside-clear-color 00000099 --inside-wrong-color 00000099 --inside-ver-color 00000099 --inside-caps-lock-color 00000099
