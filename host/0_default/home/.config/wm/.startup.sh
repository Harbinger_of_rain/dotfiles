#!/usr/bin/env sh

# turn off default screensaver
xset s off &
# turn off default standby, hibernate, ... after n minutes
xset -dpms &

# Base
setxkbmap ch &
if [ $DESKTOP_SESSION = 'plasmax11' ]; then
        bash -c "sleep 3 && killall conky" &
else
        exec ~/.screenlayout/monitors.sh &
        ~/.config/wm/polylaunch.sh &
        deadd-notification-center &
        nm-applet &
        blueman-applet &
fi 

if [ $WAYLAND_DISPLAY ]; then
        exec swhks & pkexec swhkd -c ~/.config/sxhkd/sxhkdrc_dvorak &
else
        exec sxhkd -c ~/.config/sxhkd/sxhkdrc_dvorak &
fi

xinput --set-prop --type=int --format=8 "Primax Kensington Eagle Trackball" "Evdev Middle Button Emulation" "1" &
xinput --set-prop "Primax Kensington Eagle Trackball" 'libinput Accel Profile Enabled' 0, 1 &
xinput --set-prop "Logitech Gaming Mouse G502" 'libinput Accel Profile Enabled' 0, 1 &
xinput --set-prop "ErgoDox EZ ErgoDox EZ Mouse" 'libinput Accel Profile Enabled' 0, 1 &
xinput --set-prop "OLKB Preonic Mouse" 'libinput Accel Profile Enabled' 0, 1 &
xinput --set-prop "MX Anywhere 2S Mouse" 'libinput Accel Profile Enabled' 0, 1 &
xinput --set-prop "pointer:Logitech MX Ergo" 'libinput Accel Profile Enabled' 0, 1 &

# lock when idle
swayidle -w \
        timeout 300 'bash -c ~/.config/wm/swaylock.sh' \
        timeout 600 'hyprctl dispatch dpms off' resume 'hyprctl dispatch dpms on' \
        before-sleep 'bash -c ~/.config/wm/swaylock.sh'

# Services
mpd &
screen -dmS mpcCurrent bash -c 'while true; do notify-send "Now playing" "$(mpc -p 6666 current --wait)"; done' &
#bash -c 'oneko -tora -fg black -bg darkgray -tofocus' &
#nitrogen --restore &
variety --resume &
#sleep 1; picom -b --experimental-backends &
# rclone mount fryzen: rclone/Fryzen\ -\ Archive/ &
# rclone --vfs-cache-mode writes mount fryzen: ~/rclone/fryzen/
# rclone --vfs-cache-mode writes mount gdrive: ~/rclone/gdrive/
# clipmenud &

