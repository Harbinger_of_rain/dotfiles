import re
from xkeysnail.transform import *

define_modmap({
  Key.CAPSLOCK: Key.SPACE,
  Key.GRAVE: Key.ESC
})
